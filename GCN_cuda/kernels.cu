#include "header.h"
#include <wb.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
using namespace std::chrono;
using namespace std;

#define TILE_WIDTH 32

#define wbCheck(stmt)                                                     \
  do {                                                                    \
    cudaError_t err = stmt;                                               \
    if (err != cudaSuccess) {                                             \
      wbLog(ERROR, "CUDA error: ", cudaGetErrorString(err));              \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                         \
      return -1;                                                          \
    }                                                                     \
  } while (0)


feature_t aggregation (graph_t graph_c, feature_t in_feature_c) {
	auto start = high_resolution_clock::now();
	int i, k, j;
	feature_t out_feature_c;

	printf("AGGREGATION: A[%d][%d] * X[%d][%d] = X'[%d][%d]   ", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float**) malloc (in_feature_c.feature_num * sizeof(float*));
	for (i = 0; i < in_feature_c.feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}

	auto loop_start = high_resolution_clock::now();

	for (i = 0; i < in_feature_c.node_num; ++i) {
		//printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			//for every feature in one node, compute the neighbor weighted version feature value(like a convolution)
			for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
				out_feature_c.features[k][i] += in_feature_c.features[k][graph_c.neighbours[j]];
			}
			//matrix of mean vectors
			out_feature_c.features[k][i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}

	auto stop = high_resolution_clock::now();

	auto duration_loop = duration_cast<microseconds>(stop - loop_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = (in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + in_feature_c.feature_num * in_feature_c.node_num);
	float division_number =  in_feature_c.feature_num * in_feature_c.node_num;
	float gflops =  ops / passed_time_loop / 1000000000.0;
	float gdivisions = division_number / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	float bytes = (3* 4 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + 6 * 4* in_feature_c.feature_num * in_feature_c.node_num);
	printf("   ops/bytes: %.3f", float(ops/bytes));
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   time: %.6fs\n", passed_time);

	//printf("\r\t\r");

	return out_feature_c;
}

feature_t combination (feature_t in_feature_c, parameter_t parameter_c, bool relu) {
	auto start = high_resolution_clock::now();

	int i, j, k;
	feature_t out_feature_c;

	if (in_feature_c.feature_num != parameter_c.in_feature_num) {
    	printf("ERROR: Incompatible number of features in feature (%d) and parameter (%d) objects!\n", in_feature_c.feature_num, parameter_c.in_feature_num);
    	exit(-1);
	}

	printf("COMBINATION: X'[%d][%d] * W[%d][%d] = X[%d][%d]   ", in_feature_c.node_num, in_feature_c.feature_num, parameter_c.in_feature_num, parameter_c.out_feature_num, in_feature_c.node_num, parameter_c.out_feature_num);

	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.feature_num = parameter_c.out_feature_num;
	out_feature_c.features = (float**) malloc (parameter_c.out_feature_num * sizeof(float*));
	for (i = 0; i < parameter_c.out_feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
	}

	auto loop_start = high_resolution_clock::now();

	for (i = 0; i < in_feature_c.node_num; ++i) {
		//printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (j = 0; j < parameter_c.out_feature_num; ++j) {
			out_feature_c.features[j][i] = parameter_c.biasses[j];
			for (k = 0; k < parameter_c.in_feature_num; ++k) {
				out_feature_c.features[j][i] += in_feature_c.features[k][i] * parameter_c.weights[k][j];
			}
			if(relu)
				out_feature_c.features[j][i] = MAX(0.00000, out_feature_c.features[j][i]);
		}
	}
	auto stop = high_resolution_clock::now();

	auto duration_loop = duration_cast<microseconds>(stop - loop_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = ((float)in_feature_c.node_num * parameter_c.out_feature_num * parameter_c.in_feature_num * 2)
					+ (relu? (in_feature_c.node_num * parameter_c.out_feature_num): 0);
	float gflops = ops / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);

	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   %.6fs\n", passed_time);

	

	//printf("\r\t\r");
	
	return out_feature_c;
}

void analyzer (feature_t feature_c, label_t label_c) {
	int i, j;
	int correct_num = 0;

	for (i = 0; i < feature_c.node_num; ++i) {
		float max_feature = feature_c.features[0][i];
		int max_idx = 0;
		for (j = 1; j < feature_c.feature_num; ++j) {
			if(feature_c.features[j][i] > max_feature) {
				max_feature = feature_c.features[j][i];
				max_idx = j;
			}
		}
		if (max_idx == label_c[i]) {
			correct_num++;
		}
	}
	
	printf("Accuracy: %.2f%%\n", (float)correct_num * 100.00 / (float)feature_c.node_num);
}

void analyzer2 (feature_t feature_c1, feature_t feature_c2) {
	int i, j;
	int correct_num = 0;

	for (i = 0; i < feature_c1.node_num; ++i) {
		float max_feature1 = feature_c1.features[0][i];
		int max_idx1 = 0;
		for (j = 1; j < feature_c1.feature_num; ++j) {
			if(feature_c1.features[j][i] > max_feature1) {
				max_feature1 = feature_c1.features[j][i];
				max_idx1 = j;
			}
		}
		float max_feature2 = feature_c2.features[0][i];
		int max_idx2 = 0;
		for (j = 1; j < feature_c2.feature_num; ++j) {
			if(feature_c2.features[j][i] > max_feature2) {
				max_feature2 = feature_c2.features[j][i];
				max_idx2 = j;
			}
		}
		if (max_idx1 != max_idx2) {
			printf("idx: %d\n", i);
			for (j = 0; j < feature_c1.feature_num; ++j){
				printf("%.2f ",feature_c1.features[j][i]);
			}
			printf("\n");
			for (j = 0; j < feature_c2.feature_num; ++j){
				printf("%.2f ",feature_c2.features[j][i]);
			}
			printf("\n");
		}
	}
	
	//printf("Accuracy: %.2f%%\n", (float)correct_num * 100.00 / (float)feature_c.node_num);
}

/////////////////////////////////////////
////     CSR aggregation kernel      ////
/////////////////////////////////////////

__global__ void aggregation_mul(float* X, int* A, int* index, float* out, int width, int height){
	int row = threadIdx.y +  blockDim.y * blockIdx.y;
  	int col = threadIdx.x +  blockDim.x * blockIdx.x;

	int idx = row*width+col;
	int idx2 = row*width;
	
	if(row<height && col<width){
		out[idx] = 0;
		for(int j=index[col]; j<index[col+1]; ++j){
			out[idx] += X[idx2+A[j]];
		}
		out[idx] /= ((float)index[col+1] - index[col]);
	}
}

feature_t aggregation_CSR (graph_t graph_c, feature_t in_feature_c) {
	auto start = high_resolution_clock::now();
	int i, k, j;
	feature_t out_feature_c;

	printf("AGGREGATION CSR: A[%d][%d] * X[%d][%d] = X'[%d][%d]   ", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float**) malloc (in_feature_c.feature_num * sizeof(float*));
	for (i = 0; i < in_feature_c.feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}
	

	int 	width 	= in_feature_c.node_num;
	int 	height 	= in_feature_c.feature_num;
	float* 	in_feature_c_device;
	int* 	indexes_device;
	int* 	neighbours;
	float* 	out_feature_c_device;
	//allocate in_feature matrix gpu memory
	//allocate neightbor indexes gpu memory
	//allocate neightbor gpu memory
	//allocate out_feature matrix gpu memory
	cudaMalloc((void **) &in_feature_c_device, 	height * width * sizeof(float));
	cudaMalloc((void **) &indexes_device, 		(width+1) * sizeof(int));
	cudaMalloc((void **) &neighbours, 			graph_c.indexes[width] * sizeof(int));
	cudaMalloc((void **) &out_feature_c_device, height * width * sizeof(float));

	for(i=0; i<height; ++i){
		cudaMemcpy(in_feature_c_device+i*width, in_feature_c.features[i], width * sizeof(float), cudaMemcpyHostToDevice);
	}
	cudaMemcpy(indexes_device, 	graph_c.indexes, 	(width+1) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(neighbours, 		graph_c.neighbours, graph_c.indexes[width] * sizeof(int), cudaMemcpyHostToDevice);
	
	auto device_start = high_resolution_clock::now();

	dim3 dimGrid(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock(32, 32);
	aggregation_mul<<<dimGrid, dimBlock>>>(in_feature_c_device, neighbours, indexes_device, out_feature_c_device, width, height);

	cudaDeviceSynchronize();

	auto device_stop = high_resolution_clock::now();
	
	//copy out_feature to cpu
	for (i = 0; i < height; ++i) {
		cudaMemcpy(out_feature_c.features[i], out_feature_c_device+i*width, width * sizeof(float), cudaMemcpyDeviceToHost);
	}
	
/*
	for (i = 0; i < in_feature_c.node_num; ++i) {
		printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			//for every feature in one node, compute the neighbor weighted version feature value(like a convolution)
			
			for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
				out_feature_c.features[k][i] += in_feature_c.features[k][graph_c.neighbours[j]];
			}
			
			//matrix of mean vectors
			//out_feature_c.features[k][i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}
	*/
	cudaFree(in_feature_c_device);
	cudaFree(indexes_device);
	cudaFree(neighbours);
	cudaFree(out_feature_c_device);

	auto stop = high_resolution_clock::now();
	auto duration_loop = duration_cast<microseconds>(device_stop - device_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = (2 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + in_feature_c.feature_num * in_feature_c.node_num);
	float division_number =  in_feature_c.feature_num * in_feature_c.node_num;
	
	float gflops =  ops / passed_time_loop / 1000000000.0;
	float gdivisions = division_number / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	float bytes = (3* 4 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + 6 * 4* in_feature_c.feature_num * in_feature_c.node_num);
	printf("   ops/bytes: %.3f", float(ops/bytes));
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   %.6fs\n", passed_time);

	

	//printf("\r\t\r");
	return out_feature_c;
}

/////////////////////////////////////////
//////     combination kernel      //////
/////////////////////////////////////////
__global__ void matrixMultiplyShared(float *A, float *B, float *C,
                                     int numARows, int numAColumns,
                                     int numBRows, int numBColumns,
                                     int numCRows, int numCColumns) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to use shared memory for this MP
  __shared__ float subTileM[TILE_WIDTH][TILE_WIDTH];
  __shared__ float subTileN[TILE_WIDTH][TILE_WIDTH];

  int bx = blockIdx.x; int by = blockIdx.y;
  int tx = threadIdx.x; int ty = threadIdx.y;

  int Row = by * TILE_WIDTH + ty;
  int Col = bx * TILE_WIDTH + tx;

  float Pvalue = 0;
  
  for (int m=0; m<ceil(numBRows/(float)TILE_WIDTH); ++m){
    //subTileM[ty][tx] = (m*TILE_WIDTH+tx<numAColumns)? A[Row*numAColumns + m*TILE_WIDTH+tx]: 0;
    //subTileN[ty][tx] = (m*TILE_WIDTH+ty<numBRows)? B[(m*TILE_WIDTH+ty)*numBColumns+Col]: 0;
    //subTileM[ty][tx] = A[Row*numAColumns + m*TILE_WIDTH+tx];
	//subTileM[tx][ty] = A[(m*TILE_WIDTH+tx)*numAColumns+Row];
	subTileM[tx][ty] = A[(m*TILE_WIDTH+tx)*numAColumns+Row];
    subTileN[ty][tx] = B[(m*TILE_WIDTH+ty)*numBColumns+Col];
    __syncthreads();

    for (int k = 0; k < TILE_WIDTH; ++k){
      if(m*TILE_WIDTH+k<numARows && m*TILE_WIDTH+k<numBRows && Row<numAColumns && Col<numBColumns){
        Pvalue += subTileM[k][ty] * subTileN[k][tx];
      }
    }
    __syncthreads();
  }
  if(Row < numCRows && Col < numCColumns) C[Row*numCColumns+Col] = Pvalue;
  if(Row<=0 && Col<=0) printf(" ");
}

__global__ void matrixMultiply(float* A, float* B, float* C, int numARows,
                               int numAColumns, int numBRows,
                               int numBColumns, int numCRows,
                               int numCColumns) {
  //@@ Insert code to implement matrix multiplication here
  int row = threadIdx.y +  blockDim.y * blockIdx.y;
  int col = threadIdx.x +  blockDim.x * blockIdx.x;

  if(col<numCColumns & row<numCRows) {
    float Pvalue = 0;
    for (int k=0; k<numBRows; ++k){
      float Melement = A[k*numAColumns+row];
      float Nelement = B[k*numBColumns+col];
      Pvalue += Melement * Nelement;
    }
	C[row*numCColumns+col]= Pvalue;
  }
}

__global__ void addbias(float* data, float* bias, int width, int height, bool relu){
	int row = threadIdx.y +  blockDim.y * blockIdx.y;
  	int col = threadIdx.x +  blockDim.x * blockIdx.x;
	if(col<width && row<height){
		data[row*width+col] += bias[row];
		if(relu){
			data[row*width+col] = MAX(0.00000, data[row*width+col]);
		}
	}
}

feature_t combination_gpu (feature_t in_feature_c, parameter_t parameter_c, bool relu){
	auto start = high_resolution_clock::now();
	int i, j, k;
	feature_t out_feature_c;

	if (in_feature_c.feature_num != parameter_c.in_feature_num) {
    	printf("ERROR: Incompatible number of features in feature (%d) and parameter (%d) objects!\n", in_feature_c.feature_num, parameter_c.in_feature_num);
    	exit(-1);
	}

	printf("COMBINATION: X'[%d][%d] * W[%d][%d] = X[%d][%d]   ", in_feature_c.node_num, in_feature_c.feature_num, parameter_c.in_feature_num, parameter_c.out_feature_num, in_feature_c.node_num, parameter_c.out_feature_num);

	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.feature_num = parameter_c.out_feature_num;
	out_feature_c.features = (float**) malloc (parameter_c.out_feature_num * sizeof(float*));
	for (i = 0; i < parameter_c.out_feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}

	int height = parameter_c.out_feature_num;
	int width = in_feature_c.node_num;
	float *features_device;
	//allocate device feature memory
	cudaMalloc((void**) &features_device, height * width * sizeof(float));

	float* in_feature_c_device;
	float* weights_device;
	int z = parameter_c.in_feature_num;
	//allocate in_feature matrix gpu memory
	cudaMalloc((void **) &in_feature_c_device, z*width*sizeof(float));
	//allocate weights matrix gpu memory
	cudaMalloc((void **) &weights_device, z*height*sizeof(float));
	
	for(i=0; i<z; ++i){
		cudaMemcpy(in_feature_c_device+i*width, in_feature_c.features[i], width * sizeof(float), cudaMemcpyHostToDevice);
	}
	for(i=0; i<z; ++i){
		cudaMemcpy(weights_device+i*height, parameter_c.weights[i], height * sizeof(float), cudaMemcpyHostToDevice);
	}

	auto device_start = high_resolution_clock::now();
	
	dim3 dimGrid(ceil(width/float(TILE_WIDTH)), ceil(height/float(TILE_WIDTH)));
	dim3 dimBlock(TILE_WIDTH, TILE_WIDTH);
	matrixMultiplyShared<<<dimGrid, dimBlock>>>(weights_device, in_feature_c_device, features_device, z, height, z, width, height, width);

	cudaDeviceSynchronize();

	cudaFree(weights_device);
	cudaFree(in_feature_c_device);

	float* biasses_device;
	
	//allocate device bias memory
	cudaMalloc((void **) &biasses_device, height * sizeof(float));
	cudaMemcpy(biasses_device, parameter_c.biasses, height * sizeof(float), cudaMemcpyHostToDevice);

	//bias kernel
	dim3 dimGrid1(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock1(32, 32);
	addbias<<<dimGrid1, dimBlock1>>>(features_device, biasses_device, width, height, relu);
	cudaDeviceSynchronize();

	auto device_stop = high_resolution_clock::now();

	
	
	//copy out_feature to cpu
	for (i = 0; i < height; ++i) {
		cudaMemcpy(out_feature_c.features[i], features_device+i*width, width * sizeof(float), cudaMemcpyDeviceToHost);
	}
	cudaFree(features_device);
	cudaFree(biasses_device);
/*
	for (i = 0; i < in_feature_c.node_num; ++i) {
		printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (j = 0; j < parameter_c.out_feature_num; ++j) {
			//out_feature_c.features[j][i] = parameter_c.biasses[j];
			
			for (k = 0; k < parameter_c.in_feature_num; ++k) {
				out_feature_c.features[j][i] += in_feature_c.features[k][i] * parameter_c.weights[k][j];
			}
			
			if(relu)
				out_feature_c.features[j][i] = MAX(0.00000, out_feature_c.features[j][i]);
		}
	}
	*/
	//printf("\r\t\r");

	auto stop = high_resolution_clock::now();
	
	auto duration_loop = duration_cast<microseconds>(device_stop - device_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = ((float)in_feature_c.node_num * parameter_c.out_feature_num * parameter_c.in_feature_num * 2)
					+ (relu? (in_feature_c.node_num * parameter_c.out_feature_num): 0);
	float gflops = ops / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   %.6fs\n", passed_time);

	return out_feature_c;
}


/////////////////////////////////////////
////     ELL aggregation kernel      ////
/////////////////////////////////////////

__global__ void SpMV_ELL(int width, int height, int *data, int *col_index, int ell_height, float *x, float *y) {
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;

	int idx = row*width;
	if (col < width && row < height) {
		float dot = 0;
		for (int i = 0; i < ell_height; i++) {
			int index = col+i*width;
			dot += data[index] * x[col_index[index] + idx];
		}
		y[idx + col] = dot;
	}
	//if(col<=10 && row<=10) printf("%.2f", data[row * width + col]);
}

__global__ void normalize(float* data, int* indexes, int width, int height){
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;

	if(row < height && col < width){
		data[row * width + col] /= ((float)indexes[col + 1] - indexes[col]);
	}
	//if(col<=10 && row<=10) printf("%.2f", data[row * width + col]);
}

ELL* CSR_to_ELL(graph_t graph_c, int node_num){
	ELL* ell = new ELL;
	int max_length = graph_c.indexes[1] - graph_c.indexes[0];
	for(int i=1; i<node_num; ++i){
		max_length = max(graph_c.indexes[i+1]-graph_c.indexes[i], max_length);
	}
	ell->width = node_num;
	ell->height = max_length;
	
	
	ell->data = (int**) malloc (max_length * sizeof(int*));
	ell->col  = (int**) malloc (max_length * sizeof(int*));
	for (int i = 0; i < max_length; ++i) {
		ell->data[i] = (int*) malloc (node_num * sizeof(int));
		ell->col[i]  = (int*) malloc (node_num * sizeof(int));
		for (int j = 0; j < node_num; ++j) {
			ell->data[i][j] = 0;
			ell->col[i][j] = 0;
		}
	}
	/*
	ell.data = (int*) malloc (max_length * node_num * sizeof(int));
	ell.col  = (int*) malloc (max_length * node_num * sizeof(int));

	memset(ell.data, 0, max_length * node_num * sizeof(int));
	memset(ell.col, 0, max_length * node_num * sizeof(int));
	*/
	
	for(int i=0; i<node_num; ++i){
		int k = 0;
		for(int j=graph_c.indexes[i]; j<graph_c.indexes[i+1]; ++j){
			ell->data[k][i] = 1;
			ell->col[k][i] = graph_c.neighbours[j];
			++k;
		}
	}	
	/*
	for(int i=0; i<node_num; ++i){
		int k = 0;
		for(int j=graph_c.indexes[i]; j<graph_c.indexes[i+1]; ++j){
			ell.data[k*node_num + i] = 1;
			ell.col[k*node_num + i] = graph_c.neighbours[j];
			++k;
		}
	}	
*/
	return ell;
}

feature_t aggregation_ELL (graph_t graph_c, feature_t in_feature_c, ELL* ell) {
	int i, k, j;
	feature_t out_feature_c;

	auto start = high_resolution_clock::now();

	printf("AGGREGATION ELL: A[%d][%d] * X[%d][%d] = X'[%d][%d]   ", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float**) malloc (in_feature_c.feature_num * sizeof(float*));
	for (i = 0; i < in_feature_c.feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}

	//ELL ell = CSR_to_ELL(graph_c, in_feature_c.node_num);

	int 	width 	= in_feature_c.node_num;
	int 	height 	= in_feature_c.feature_num;
	float* 	in_feature_c_device;
	int* 	data_device;
	int* 	col_device;
	float* 	out_feature_c_device;
	int*    indexes_device;
	//allocate in_feature matrix gpu memory
	//allocate neightbor indexes gpu memory
	//allocate neightbor gpu memory
	//allocate out_feature matrix gpu memory
	printf("need GPU memory: %.2f", ((width+1) * sizeof(int) + 2 * (height * width * sizeof(float) + ell->width * ell->height * sizeof(int)))/1024.0/1024.0/1024.0);
	cudaMalloc((void **) &in_feature_c_device, 	height * width * sizeof(float));
	cudaMalloc((void **) &data_device, 		ell->width * ell->height * sizeof(int));
	cudaMalloc((void **) &col_device, 			ell->width * ell->height * sizeof(int));
	cudaMalloc((void **) &out_feature_c_device, height * width * sizeof(float));
	cudaMalloc((void **) &indexes_device, 		(width+1) * sizeof(int));

	for(i=0; i<ell->height; ++i){
		cudaMemcpy(data_device + i*ell->width, ell->data[i], ell->width * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(col_device  + i*ell->width, ell->col[i],  ell->width * sizeof(int), cudaMemcpyHostToDevice);
	}
	
	for(i=0; i<height; ++i){
		cudaMemcpy(in_feature_c_device+i*width, in_feature_c.features[i], width * sizeof(float), cudaMemcpyHostToDevice);
	}

	
/*
	cudaMemcpy(data_device, ell.data, ell.width * ell.height * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(col_device, ell.col, ell.width * ell.height * sizeof(int), cudaMemcpyHostToDevice);
*/
	cudaMemcpy(indexes_device, 	graph_c.indexes, 	(width+1) * sizeof(int), cudaMemcpyHostToDevice);
	auto loop_start = high_resolution_clock::now();

	dim3 dimGrid(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock(32, 32);
	SpMV_ELL<<<dimGrid, dimBlock>>>(width, height, data_device, col_device, ell->height, in_feature_c_device, out_feature_c_device);

	cudaDeviceSynchronize();
	cudaFree(in_feature_c_device);
	cudaFree(data_device);
	cudaFree(col_device);
	auto stop = high_resolution_clock::now();

	normalize<<<dimGrid, dimBlock>>>(out_feature_c_device, indexes_device, width, height);

	cudaDeviceSynchronize();

	//copy out_feature to cpu
	for (i = 0; i < height; ++i) {
		cudaMemcpy(out_feature_c.features[i], out_feature_c_device+i*width, width * sizeof(float), cudaMemcpyDeviceToHost);
	}


/*
	for (i = 0; i < in_feature_c.node_num; ++i) {
		//printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    //fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			//for every feature in one node, compute the neighbor weighted version feature value(like a convolution)
			
			//for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
			//	out_feature_c.features[k][i] += in_feature_c.features[k][graph_c.neighbours[j]];
			//}
			
			//matrix of mean vectors
			out_feature_c.features[k][i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}
*/
	
	cudaFree(out_feature_c_device);
	cudaFree(indexes_device);

/*
	for (int i = 0; i < ell->height; ++i) {
		//ell.data[i] = (int*) malloc (node_num * sizeof(int));
		//ell.col[i]  = (int*) malloc (node_num * sizeof(int));
		free((void*)ell->data[i]);
		free((void*)ell->col[i]);
	}
	*/

	auto duration_loop = duration_cast<microseconds>(stop - loop_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float division_number =  in_feature_c.feature_num * in_feature_c.node_num;
	float gflops = (ceil(width/32.0) * ceil(height/32.0) * 1024.0 * (4 * ell->height)) / passed_time_loop / 1000000000.0;
	float gdivisions = division_number / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	float bytes = ((3 * ell->height + 1) * 4.0 * ceil(width/32.0) * ceil(height/32.0) * 1024 + 3 * 4* in_feature_c.feature_num * in_feature_c.node_num);
	printf("   ops/bytes: %.3f", float((ceil(width/32.0) * ceil(height/32.0) * 1024.0 * (4 * ell->height))/bytes));
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   %.6fs\n", passed_time);

	//printf("\r\t\r");
	return out_feature_c;
}


/////////////////////////////////////////
////     JDS aggregation kernel      ////
/////////////////////////////////////////

__global__ void aggregation_jds(float* X, int* A, int* index, float* out, int width, int height, int* jds_row_index){
	int row = threadIdx.y +  blockDim.y * blockIdx.y;
  	int col = threadIdx.x +  blockDim.x * blockIdx.x;

	int idx = row*width+jds_row_index[col];
	int idx2 = row*width;
	if(row<height && col<width){
		out[idx] = 0;
		for(int j=index[col]; j<index[col+1]; ++j){
			out[idx] += X[idx2+A[j]];
		}
	}
}


bool cmp(const std::pair<int, int> &a, const std::pair<int, int> &b){
	return (a.first >= b.first);
}

JDS* CSR_to_JDS(graph_t graph_c, int node_num){
	JDS* jds = new JDS;
	vector<pair<int, int>> v;
	for(int i=0; i<node_num; ++i){
		v.push_back(make_pair(graph_c.indexes[i+1] - graph_c.indexes[i], i));
		//v[i].first = graph_c.indexes[i+1] - graph_c.indexes[i];
		//v[i].second = i;
	}
	jds->col_index = (int*) malloc (graph_c.indexes[node_num] * sizeof(int));
	jds->jds_row_ptr = (int*) malloc ((node_num+1) * sizeof(int));
	jds->jds_row_index = (int*) malloc (node_num * sizeof(int));
	jds->jds_row_ptr[0] = 0;

	stable_sort(v.begin(), v.end(), cmp);
	
	int k=0;
	for(int i=0; i<node_num; ++i){
		int idx=v[i].second;
		jds->jds_row_index[i] = idx;
		jds->jds_row_ptr[i+1] = jds->jds_row_ptr[i] + v[i].first;
		for(int j=graph_c.indexes[idx]; j<graph_c.indexes[idx+1]; ++j){
			jds->col_index[k] = graph_c.neighbours[j];
			++k;
		}
	}
	return jds;
}

feature_t aggregation_JDS (graph_t graph_c, feature_t in_feature_c, JDS* jds) {
	auto start = high_resolution_clock::now();
	int i, k, j;
	feature_t out_feature_c;

	printf("AGGREGATION CSR: A[%d][%d] * X[%d][%d] = X'[%d][%d]   ", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float**) malloc (in_feature_c.feature_num * sizeof(float*));
	for (i = 0; i < in_feature_c.feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}

	//JDS jds = CSR_to_JDS(graph_c, in_feature_c.node_num);

	int 	width 	= in_feature_c.node_num;
	int 	height 	= in_feature_c.feature_num;
	float* 	in_feature_c_device;
	int* 	indexes_device;
	int* 	neighbours;
	float* 	out_feature_c_device;
	int*    jds_col_index_device;
	//allocate in_feature matrix gpu memory
	//allocate neightbor indexes gpu memory
	//allocate neightbor gpu memory
	//allocate out_feature matrix gpu memory
	cudaMalloc((void **) &in_feature_c_device, 	height * width * sizeof(float));
	cudaMalloc((void **) &indexes_device, 		(width+1) * sizeof(int));
	cudaMalloc((void **) &neighbours, 			graph_c.indexes[width] * sizeof(int));
	cudaMalloc((void **) &out_feature_c_device, height * width * sizeof(float));
	cudaMalloc((void **) &jds_col_index_device, width * sizeof(int));

	for(i=0; i<height; ++i){
		cudaMemcpy(in_feature_c_device+i*width, in_feature_c.features[i], width * sizeof(float), cudaMemcpyHostToDevice);
	}
	cudaMemcpy(indexes_device, 	jds->jds_row_ptr, 	(width+1) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(neighbours, 		jds->col_index, graph_c.indexes[width] * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(jds_col_index_device, 		jds->jds_row_index, width * sizeof(int), cudaMemcpyHostToDevice);
	
	auto device_start = high_resolution_clock::now();

	dim3 dimGrid(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock(32, 32);
	aggregation_jds<<<dimGrid, dimBlock>>>(in_feature_c_device, neighbours, indexes_device, out_feature_c_device, width, height, jds_col_index_device);

	cudaDeviceSynchronize();
	
	cudaMemcpy(indexes_device, 	graph_c.indexes, 	(width+1) * sizeof(int), cudaMemcpyHostToDevice);

	dim3 dimGrid2(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock2(32, 32);

	normalize<<<dimGrid2, dimBlock2>>>(out_feature_c_device, indexes_device, width, height);

	cudaDeviceSynchronize();
	
	auto device_stop = high_resolution_clock::now();
	

	//copy out_feature to cpu
	for (i = 0; i < height; ++i) {
		cudaMemcpy(out_feature_c.features[i], out_feature_c_device+i*width, width * sizeof(float), cudaMemcpyDeviceToHost);
	}
	fflush(stdout);
/*
	for (i = 0; i < in_feature_c.node_num; ++i) {
		printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			//for every feature in one node, compute the neighbor weighted version feature value(like a convolution)
			
			//for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
			//	out_feature_c.features[k][i] += in_feature_c.features[k][graph_c.neighbours[j]];
			//}
			
			//matrix of mean vectors
			out_feature_c.features[k][i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}
	*/
	
	cudaFree(in_feature_c_device);
	cudaFree(indexes_device);
	cudaFree(neighbours);
	cudaFree(out_feature_c_device);
	cudaFree(jds_col_index_device);

/*
	free((void*)jds.col_index);
	free((void*)jds.jds_row_ptr);
	free((void*)jds.jds_row_index);
	*/
	auto stop = high_resolution_clock::now();

	auto duration_loop = duration_cast<microseconds>(device_stop - device_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = (2 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + in_feature_c.feature_num * in_feature_c.node_num);
	float division_number =  in_feature_c.feature_num * in_feature_c.node_num;
	float gflops =  ops / passed_time_loop / 1000000000.0;
	float gdivisions = division_number / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	float bytes = (3* 4 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + 7 * 4* in_feature_c.feature_num * in_feature_c.node_num);
	printf("   ops/bytes: %.3f", float(ops/bytes));
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	printf("   %.6fs", passed_time_loop);
	printf("   %.6fs\n", passed_time);

	

	//printf("\r\t\r");
	return out_feature_c;
}


/////////////////////////////////////////
////     COO aggregation kernel      ////
/////////////////////////////////////////

__global__ void aggregation_coo(int* col_index, int* row_index, int width, int height, int coo_width, float* X, float* out){
	int row = threadIdx.y +  blockDim.y * blockIdx.y;
  	int col = threadIdx.x +  blockDim.x * blockIdx.x;

	if(row < height && col < coo_width){
		atomicAdd(&out[row*width+row_index[col]], X[row*width+col_index[col]]);
	}
}

COO* CSR_to_COO(graph_t graph_c, int node_num){
	COO* coo = new COO;
	coo->col_index = (int*) malloc (graph_c.indexes[node_num] * sizeof(int));
	coo->row_index = (int*) malloc (graph_c.indexes[node_num] * sizeof(int));
	int k=0;
	for(int i=0; i<node_num; ++i){
		for(int j=graph_c.indexes[i]; j<graph_c.indexes[i+1]; ++j){
			coo->col_index[k] = graph_c.neighbours[j];
			coo->row_index[k] = i;
			++k;
		}
	}
	return coo;
}

feature_t aggregation_COO (graph_t graph_c, feature_t in_feature_c, COO* coo) {
	auto start = high_resolution_clock::now();
	int i, k, j;
	feature_t out_feature_c;

	printf("AGGREGATION CSR: A[%d][%d] * X[%d][%d] = X'[%d][%d]   ", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float**) malloc (in_feature_c.feature_num * sizeof(float*));
	for (i = 0; i < in_feature_c.feature_num; ++i) {
		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i][j] = 0;
		}
	}

	//COO coo = CSR_to_COO(graph_c, in_feature_c.node_num);

	int 	width 	= in_feature_c.node_num;
	int 	height 	= in_feature_c.feature_num;
	float* 	in_feature_c_device;
	int* 	col_index;
	int* 	row_index;
	float* 	out_feature_c_device;
	//allocate in_feature matrix gpu memory
	//allocate neightbor indexes gpu memory
	//allocate neightbor gpu memory
	//allocate out_feature matrix gpu memory
	cudaMalloc((void **) &in_feature_c_device, 	height * width * sizeof(float));
	cudaMalloc((void **) &col_index, 		graph_c.indexes[width] * sizeof(int));
	cudaMalloc((void **) &row_index, 			graph_c.indexes[width] * sizeof(int));
	cudaMalloc((void **) &out_feature_c_device, height * width * sizeof(float));

	cudaMemset(out_feature_c_device, 0, height * width * sizeof(float));

	for(i=0; i<height; ++i){
		cudaMemcpy(in_feature_c_device+i*width, in_feature_c.features[i], width * sizeof(float), cudaMemcpyHostToDevice);
	}
	cudaMemcpy(col_index, 	coo->col_index, 	graph_c.indexes[width] * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(row_index, 		coo->row_index, graph_c.indexes[width] * sizeof(int), cudaMemcpyHostToDevice);
	
	auto device_start = high_resolution_clock::now();

	dim3 dimGrid(ceil(graph_c.indexes[width]/32.0), ceil(height/32.0));
	dim3 dimBlock(32, 32);
	aggregation_coo<<<dimGrid, dimBlock>>>(col_index, row_index, width, height, graph_c.indexes[width], in_feature_c_device, out_feature_c_device);

	cudaDeviceSynchronize();

	int*    indexes_device;
	cudaMalloc((void **) &indexes_device, 		(width+1) * sizeof(int));
	cudaMemcpy(indexes_device, 	graph_c.indexes, 	(width+1) * sizeof(int), cudaMemcpyHostToDevice);


	cudaFree(in_feature_c_device);
	cudaFree(col_index);
	cudaFree(row_index);


	dim3 dimGrid2(ceil(width/32.0), ceil(height/32.0));
	dim3 dimBlock2(32, 32);

	normalize<<<dimGrid2, dimBlock2>>>(out_feature_c_device, indexes_device, width, height);

	cudaDeviceSynchronize();
	

	auto device_stop = high_resolution_clock::now();

	//copy out_feature to cpu
	for (i = 0; i < height; ++i) {
		cudaMemcpy(out_feature_c.features[i], out_feature_c_device+i*width, width * sizeof(float), cudaMemcpyDeviceToHost);
	}
	
/*
	for (i = 0; i < in_feature_c.node_num; ++i) {
		//printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    //fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			//for every feature in one node, compute the neighbor weighted version feature value(like a convolution)
			
			//for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
			//	out_feature_c.features[k][i] += in_feature_c.features[k][graph_c.neighbours[j]];
			//}
			
			//matrix of mean vectors
			out_feature_c.features[k][i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}
	*/
	
	
	cudaFree(out_feature_c_device);
	cudaFree(indexes_device);

	auto stop = high_resolution_clock::now();

	auto duration_loop = duration_cast<microseconds>(device_stop - device_start);
	float passed_time_loop = duration_loop.count()/1000000.0;
	float ops = (2 * in_feature_c.feature_num * (float)graph_c.indexes[in_feature_c.node_num] + in_feature_c.feature_num * in_feature_c.node_num);
	float division_number =  in_feature_c.feature_num * in_feature_c.node_num;
	float gflops =  ops / passed_time_loop / 1000000000.0;
	float gdivisions = division_number / passed_time_loop / 1000000000.0;
	printf("GFLOPS: %.3f", gflops);
	float bytes = (4* 4 * ceil(graph_c.indexes[width]/32.0)*ceil(height/32.0)*1024 + 3 * 4* in_feature_c.feature_num * in_feature_c.node_num);
	printf("   ops/bytes: %.3f", float(ops/bytes));
	
	auto duration = duration_cast<microseconds>(stop - start);
	float passed_time = duration.count()/1000000.0;
	
	printf("   %.6fs\n", passed_time);
	

	

	//printf("\r\t\r");
	return out_feature_c;
}