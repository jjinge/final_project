#include "header.h"
#include <wb.h>

#include <chrono>
using namespace std::chrono;

#define wbCheck(stmt)                                                     \
  do {                                                                    \
    cudaError_t err = stmt;                                               \
    if (err != cudaSuccess) {                                             \
      wbLog(ERROR, "CUDA error: ", cudaGetErrorString(err));              \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                         \
      return -1;                                                          \
    }                                                                     \
  } while (0)

int main(int argc, char const *argv[]) {
	auto start = high_resolution_clock::now();

	if ((argc != 3) || ((strcmp(argv[1], "cora") != 0) && (strcmp(argv[1], "citeseer") != 0) && (strcmp(argv[1], "reddit") != 0))
		|| ((strcmp(argv[2], "baseline") != 0) && (strcmp(argv[2], "csr") != 0) && (strcmp(argv[2], "ell") != 0)
		 && (strcmp(argv[2], "coo") != 0) && (strcmp(argv[2], "jds") != 0))) {
		printf("ERROR: usage \"%s [cora|citeseer|reddit] [baseline|csr|ell|coo|jds]\"\n", argv[0]);
		return -1;
	}
	printf("dataset: %s test start\n\n", argv[1]);
	string s = argv[2];
	//data prepare
	GCN_t GCN_c = GCN_parser((char*)argv[1]);
	ELL* ell = new ELL;
	COO* coo = new COO;
	JDS* jds = new JDS;
	if(s == "ell"){
		ell = CSR_to_ELL(GCN_c.graph_c, GCN_c.feature_c.node_num);
	}
	else if(s == "coo"){
		coo = CSR_to_COO(GCN_c.graph_c, GCN_c.feature_c.node_num);
	}
	else if(s == "jds"){
		jds = CSR_to_JDS(GCN_c.graph_c, GCN_c.feature_c.node_num);
	}

	//start GCN forward
	
	if(s == "baseline"){
		printf("-------start baseline code running-------\n");
		feature_t feature_c;
		feature_c = aggregation(GCN_c.graph_c, GCN_c.feature_c);
		feature_c = combination(feature_c, GCN_c.l1_parameter_c, true);
		feature_c = aggregation(GCN_c.graph_c, feature_c);
		feature_c = combination(feature_c, GCN_c.l2_parameter_c, false);
		printf("\nbaseline ");
		analyzer(feature_c, GCN_c.label_c);

		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		float passed_time = duration.count()/1000000.0;
		printf("baseline total running time: %.6fs\n", passed_time);
		printf("-------stop baseline code running-------\n");
	}
	else if(s == "csr"){
		printf("\n\n\n-------start cuda version code running-------\n");
		printf("aggregation kernel: csr\n");
		feature_t feature_cuda;
		feature_cuda = aggregation_CSR(GCN_c.graph_c, GCN_c.feature_c);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l1_parameter_c, true);
		feature_cuda = aggregation_CSR(GCN_c.graph_c, feature_cuda);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l2_parameter_c, false);
		printf("\ncuda version ");
		analyzer(feature_cuda, GCN_c.label_c);

		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		float passed_time = duration.count()/1000000.0;
		printf("cuda version total running time: %.6fs\n", passed_time);

		printf("-------stop cuda version code running-------\n");
	}
	else if(s == "ell"){

		printf("\n\n\n-------start cuda version code running-------\n");
		printf("aggregation kernel: ell\n");
		feature_t feature_cuda;
		feature_cuda = aggregation_ELL(GCN_c.graph_c, GCN_c.feature_c, ell);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l1_parameter_c, true);
		feature_cuda = aggregation_ELL(GCN_c.graph_c, feature_cuda, ell);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l2_parameter_c, false);
		printf("\ncuda version ");
		analyzer(feature_cuda, GCN_c.label_c);

		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		float passed_time = duration.count()/1000000.0;
		printf("cuda version total running time: %.6fs\n", passed_time);

		printf("-------stop cuda version code running-------\n");
	}
	else if(s == "coo"){

		printf("\n\n\n-------start cuda version code running-------\n");
		printf("aggregation kernel: coo\n");
		feature_t feature_cuda;
		feature_cuda = aggregation_COO(GCN_c.graph_c, GCN_c.feature_c, coo);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l1_parameter_c, true);
		feature_cuda = aggregation_COO(GCN_c.graph_c, feature_cuda, coo);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l2_parameter_c, false);
		printf("\ncuda version ");
		analyzer(feature_cuda, GCN_c.label_c);

		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		float passed_time = duration.count()/1000000.0;
		printf("cuda version total running time: %.6fs\n", passed_time);

		printf("-------stop cuda version code running-------\n");
	}
	else if(s == "jds"){

		printf("\n\n\n-------start cuda version code running-------\n");
		printf("aggregation kernel: jds\n");
		feature_t feature_cuda;
		feature_cuda = aggregation_JDS(GCN_c.graph_c, GCN_c.feature_c, jds);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l1_parameter_c, true);
		feature_cuda = aggregation_JDS(GCN_c.graph_c, feature_cuda, jds);
		feature_cuda = combination_gpu(feature_cuda, GCN_c.l2_parameter_c, false);
		printf("\ncuda version ");
		analyzer(feature_cuda, GCN_c.label_c);

		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		float passed_time = duration.count()/1000000.0;
		printf("cuda version total running time: %.6fs\n", passed_time);

		printf("-------stop cuda version code running-------\n");
	}

	//analyzer2(feature_c, feature_cuda);

	return 0;
}