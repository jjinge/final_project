# GCN cuda version

## usage
1. module load cuda
2. ./build main.cu kernels.cu utilities.cu outbinary
3. sbatch run_tests outbinary cora/citeseer/reddit baseline/csr/ell/coo/jds
4. read the result in slurm-xxx.out

## note
1. the default cpu memory usage is 64000M, this will cause long waiting time. For cora and citeseer dataset, 1000M memory is enough. For reddit dataset, keep 64000M memory usage.

## todo
