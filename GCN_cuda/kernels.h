#ifndef __GCN_H__
#define __GCN_H__
#include <iostream>

feature_t aggregation (graph_t graph_c, feature_t in_feature_c);

feature_t combination (feature_t in_feature_c, parameter_t parameter_c, bool relu);

feature_t combination_gpu (feature_t in_feature_c, parameter_t parameter_c, bool relu);

feature_t aggregation_CSR (graph_t graph_c, feature_t in_feature_c);

void analyzer (feature_t feature_c, label_t label_c);

void analyzer2 (feature_t feature_c1, feature_t feature_c2);

ELL* CSR_to_ELL(graph_t graph_c, int node_num);

feature_t aggregation_ELL (graph_t graph_c, feature_t in_feature_c, ELL* ell);

bool cmp(const std::pair<int, int> &a, const std::pair<int, int> &b);

JDS* CSR_to_JDS(graph_t graph_c, int node_num);

feature_t aggregation_JDS (graph_t graph_c, feature_t in_feature_c, JDS* jds);

COO* CSR_to_COO(graph_t graph_c, int node_num);

feature_t aggregation_COO (graph_t graph_c, feature_t in_feature_c, COO* coo);

#endif