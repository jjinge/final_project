# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/kernels.c" "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/_build/CMakeFiles/GCN.dir/kernels.c.o"
  "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/main.c" "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/_build/CMakeFiles/GCN.dir/main.c.o"
  "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/utilities.c" "/scratch/eecs498f21_class_root/eecs498f21_class/shengs/GCN/_build/CMakeFiles/GCN.dir/utilities.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
